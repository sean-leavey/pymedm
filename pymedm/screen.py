"""MEDM screen"""

from .objects import Display
from .colors import DefaultColorMap


class Screen:
    def __init__(self, path, x=0, y=0, width=None, height=None, fg_color=14, bg_color=23):
        self.path = path
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.fg_color = fg_color
        self.bg_color = bg_color

        self.children = []

    @property
    def children_x_min(self):
        """Top left corner x-coordinate"""
        min_x = float('inf')
        for item in self.children:
            if item.x < min_x:
                min_x = item.x
        return min_x

    @property
    def children_y_min(self):
        """Top left corner y-coordinate"""
        min_y = float('inf')
        for item in self.children:
            if item.y < min_y:
                min_y = item.y
        return min_y

    @property
    def children_x_max(self):
        """Bottom right corner x-coordinate"""
        max_x = float('-inf')
        for item in self.children:
            x = item.x + item.width
            if x > max_x:
                max_x = x
        return max_x

    @property
    def children_y_max(self):
        """Bottom right corner y-coordinate"""
        max_y = float('-inf')
        for item in self.children:
            y = item.y + item.height
            if y > max_y:
                max_y = y
        return max_y

    @property
    def children_width(self):
        """Total children width"""
        return self.children_x_max - self.children_x_min
    
    @property
    def children_height(self):
        """Total children height"""
        return self.children_y_max - self.children_y_min

    def add(self, item):
        self.children.append(item)

    def tight_layout(self, padding=0):
        """Set screen width and height to enclose contents with specified padding"""
        x_min = self.children_x_min
        y_min = self.children_y_min

        for item in self.children:
            # Move everything w.r.t. origin and apply padding.
            item.translate(padding - x_min, padding - y_min)
        
        self.width = self.children_width + 2 * padding
        self.height = self.children_height + 2 * padding

    def dump(self):
        f = File(self.path)
        d = Display(self.x, self.y, self.width, self.height,
                    fg_color=self.fg_color, bg_color=self.bg_color)
        c = DefaultColorMap()

        items = [f, d, c] + self.children
        
        return "\n".join([obj.dump() for obj in items])


class File:
    def __init__(self, path, version="030109"):
        self.path = path
        self.version = version
    
    def dump(self):
        return f"""file {{
    name="{self.path}"
    version={self.version}
}}"""
