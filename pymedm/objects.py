"""MEDM objects"""

import abc
import textwrap

class DisplayObject(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def dump(self):
        raise NotImplementedError


class Positionable(metaclass=abc.ABCMeta):
    """Base MEDM object"""
    def __init__(self, x, y, width, height, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    @property
    def x_min(self):
        """Top left corner x-coordinate"""
        return self.x

    @property
    def y_min(self):
        """Top left corner y-coordinate"""
        return self.y

    @property
    def x_max(self):
        """Bottom right corner x-coordinate"""
        return self.x + self.width

    @property
    def y_max(self):
        """Bottom right corner y-coordinate"""
        return self.y + self.height

    @property
    def x_center(self):
        return self.x + self.width // 2
    
    @property
    def y_center(self):
        return self.y + self.height // 2

    def translate(self, x, y):
        self.x += x
        self.y += y

    def object_dump(self, indents=0):
        text = f"""object {{
    x={self.x}
    y={self.y}
    width={self.width}
    height={self.height}
}}"""

        return textwrap.indent(text, "    " * indents)
        

class Alignable(metaclass=abc.ABCMeta):
    """Alignable object"""
    def __init__(self, *args, alignment=None, **kwargs):
        super().__init__(*args, **kwargs)

        if alignment is None:
            alignment = "left"

        self._alignment = None
        self.alignment = alignment

    @property
    def alignment(self):
        return f"horiz. {self._alignment}"

    @alignment.setter
    def alignment(self, alignment):
        if alignment not in ["left", "right", "center"]:
            raise ValueError(f"invalid alignment '{alignment}'")
        self._alignment = alignment


class Colorable(metaclass=abc.ABCMeta):
    """Colorable object"""
    def __init__(self, *args, fg_color=None, bg_color=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.fg_color = fg_color
        self.bg_color = bg_color

    def color_dump(self, indents=0):
        text = f"""clr={self.fg_color}
bclr={self.bg_color}"""

        return textwrap.indent(text, "    " * indents)


class Group(DisplayObject):
    """Group of display objects"""
    def __init__(self):
        self.items = {}
    
    def add(self, name, item):
        self.items[name] = item
    
    @property
    def x(self):
        return self.x_min

    @x.setter
    def x(self, x):
        dx = x - self.x_min
        for item in self.items.values():
            item.x += dx

    @property
    def y(self):
        return self.y_min

    @y.setter
    def y(self, y):
        dy = y - self.y_min
        for item in self.items.values():
            item.y += dy

    @property
    def x_min(self):
        """Top left corner x-coordinate"""
        min_x = float('inf')
        for item in self.items.values():
            if item.x < min_x:
                min_x = item.x
        return min_x

    @property
    def y_min(self):
        """Top left corner y-coordinate"""
        min_y = float('inf')
        for item in self.items.values():
            if item.y < min_y:
                min_y = item.y
        return min_y

    @property
    def x_max(self):
        """Bottom right corner x-coordinate"""
        max_x = float('-inf')
        for item in self.items.values():
            x = item.x + item.width
            if x > max_x:
                max_x = x
        return max_x

    @property
    def y_max(self):
        """Bottom right corner y-coordinate"""
        min_y = float('-inf')
        for item in self.items.values():
            y = item.y + item.height
            if y > min_y:
                min_y = y
        return min_y

    @property
    def x_center(self):
        return self.x + self.width // 2
    
    @property
    def y_center(self):
        return self.y + self.height // 2

    @property
    def width(self):
        """Total group width"""
        return self.x_max - self.x_min
    
    @property
    def height(self):
        """Total group height"""
        return self.y_max - self.y_min

    def translate(self, x, y):
        """Translate group items"""
        for item in self.items.values():
            item.x += x
            item.y += y

    def dump(self):
        return "\n".join([item.dump() for item in self.items.values()])


class Display(DisplayObject, Positionable, Colorable):
    def dump(self):
        return f"""display {{
{self.object_dump(1)}
{self.color_dump(1)}
    cmap=""
    gridSpacing=5
    gridOn=0
    snapToGrid=0
}}"""


class Text(DisplayObject, Positionable, Alignable):
    """Text label"""
    def __init__(self, label, *args, color=1, **kwargs):
        super().__init__(*args, **kwargs)

        self.label = label
        self.color = color
    
    def dump(self):
        return f"""text {{
{self.object_dump(1)}
    "basic attribute" {{
            clr={self.color}
    }}
    textix="{self.label}"
    align="{self.alignment}"
}}
"""


class Byte(DisplayObject, Positionable, Colorable):
    """Byte monitor"""
    def __init__(self, channel, *args, fg_color=15, bg_color=20, **kwargs):
        super().__init__(*args, fg_color=fg_color, bg_color=bg_color, **kwargs)

        self.channel = channel
    
    def dump(self):
        return f"""byte {{
{self.object_dump(1)}
    monitor {{
        chan="{self.channel}"
{self.color_dump(2)}
    }}
    sbit=0
}}
"""


class ChoiceButton(DisplayObject, Positionable, Colorable):
    """Choice button"""
    def __init__(self, channel, *args, fg_color=14, bg_color=5, **kwargs):
        super().__init__(*args, fg_color=fg_color, bg_color=bg_color, **kwargs)

        self.channel = channel
    
    def dump(self):
        return f""""choice button" {{
{self.object_dump(1)}
    control {{
        chan="{self.channel}"
{self.color_dump(2)}
    }}
}}
"""