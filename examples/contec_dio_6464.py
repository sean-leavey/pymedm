"""
Digital I/O MEDM screen generator

Sean Leavey
"""

from copy import deepcopy
from pymedm import Screen
from pymedm.objects import Group, Text, Byte, ChoiceButton


if __name__ == "__main__":
    # File path.
    path = "/home/edu/medm/DIO.adl"

    # Screen title.
    title = "Contec PIO-64/64 digital input/output card ($(FE))"

    # Channels.
    input_channel_fmt = "$(IFO):$(MDL)-DIO_I%s%i"
    output_channel_fmt = "$(IFO):$(MDL)-DIO_O%s%i"

    flag_width = 30
    flag_height = 30
    flag_text_width = 10
    flag_text_height = 10
    
    choice_width = 40
    choice_height = 30
    choice_mon_width = 5
    choice_text_width = 10
    choice_text_height = 10
    
    set_gap = 30
    set_title_margin = 30
    channel_margin = 5

    def input_fieldset(word, title):
        input_set = Group()
        
        # Input channel.
        input_channel = Group()
        input_channel.add("byte", Byte("", x=0, y=0, width=flag_width, height=flag_height))
        input_channel.add("text", Text("", x=flag_width + 4, y=flag_height // 2 - flag_text_height // 2,
                                    width=flag_text_width, height=flag_text_height))

        for i in range(0, 16):
            for j in range(0, 2):
                channel_num = j * 16 + i + 1
                channel = input_channel_fmt % (word, channel_num)
                start_x = (input_channel.width + channel_margin) * j
                start_y = (input_channel.height + channel_margin) * i

                grp = deepcopy(input_channel)
                grp.translate(start_x, start_y)
                grp.items["byte"].channel = channel
                grp.items["text"].label = channel_num

                input_set.add(channel_num, grp)
        
        input_set.add("title", Text(title, x=0, y=-set_title_margin, width=50, height=18))

        return input_set

    def output_fieldset(word, title):
        output_set = Group()

        # Output channel.
        output_channel = Group()
        output_channel.add("byte", Byte("", x=0, y=0, width=choice_mon_width, height=choice_height))
        output_channel.add("button", ChoiceButton("", x=choice_mon_width, y=0, width=choice_width,
                                                height=choice_height))
        output_channel.add("text", Text("", x=choice_mon_width + choice_width + 4,
                                        y=choice_height // 2 - choice_text_height // 2,
                                        width=choice_text_width, height=choice_text_height))

        for i in range(0, 16):
            for j in range(0, 2):
                channel_num = j * 16 + i + 1
                channel = output_channel_fmt % (word, channel_num)
                start_x = (output_channel.width + channel_margin) * j
                start_y = (output_channel.height + channel_margin) * i
                
                grp = deepcopy(output_channel)
                grp.translate(start_x, start_y)
                grp.items["byte"].channel = channel
                grp.items["button"].channel = channel
                grp.items["text"].label = channel_num

                output_set.add(channel_num, grp)
        
        # Add title.
        output_set.add("title", Text(title, x=0, y=-set_title_margin, width=50, height=18))

        return output_set

    # Input/output fieldsets.
    input_set_low = input_fieldset("L", "Inputs (LOW)")
    input_set_high = input_fieldset("H", "Inputs (HIGH)")
    output_set_low = output_fieldset("L", "Outputs (LOW)")
    output_set_high = output_fieldset("H", "Outputs (HIGH)")

    # Stack sets horizontally.
    input_set_high.translate(input_set_low.x_max + set_gap, 0)
    output_set_low.translate(input_set_high.x_max + set_gap, 0)
    output_set_high.translate(output_set_low.x_max + set_gap, 0)

    main_title = Text(title, x=0, y=input_set_low.y - set_title_margin, width=300, height=18)

    screen = Screen(path)

    screen.add(main_title)
    screen.add(input_set_low)
    screen.add(input_set_high)
    screen.add(output_set_low)
    screen.add(output_set_high)

    screen.tight_layout(padding=10)

    print(screen.dump())