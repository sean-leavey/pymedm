from setuptools import setup

__version__ = "0.1.0"

setup(
    name="pymedm",
    version=__version__,
    description="MEDM screen generator",
    author="Sean Leavey",
    author_email="sean.leavey@ligo.org",
    url="https://git.ligo.org/sean-leavey/pymedm",
    license="GPLv3",
)
