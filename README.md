# pymedm
MEDM screen generator

## Description
This library lets you generate MEDM screens programmatically. It supports
text, choice buttons and byte objects, and provides a grouping object
to allow collections of objects to be moved together, replicated and
tiled across the screen in rows, columns and grids. You can ask items
or groups of items for their size and position to allow you to e.g.
center a text field above a group of objects. You can also automatically
compute the size of the screen to fit the contents with some padding around
the edges.

## Examples
See the `examples` directory for the script that produces this screen for a
64 input, 64 output digital I/O card:

![Digital I/O card](examples/contec_dio_6464.png)

## Credits
Sean Leavey  
<sean.leavey@ligo.org>